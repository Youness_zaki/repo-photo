package fr.youness.myapplication

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import fr.youness.myapplication.adapter.PhotoAdapter
import fr.youness.myapplication.core.IPhotoApi
import fr.youness.myapplication.core.ServiceBuilder
import fr.youness.myapplication.model.ListPhoto
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getPhotoList()
    }

    private fun getPhotoList() {
        val request = ServiceBuilder.buildService(IPhotoApi::class.java)
        val call = request.getEmployees()
        call.enqueue(object : Callback<ListPhoto> {
            override fun onResponse(
                call: Call<ListPhoto>,
                response: Response<ListPhoto>
            ) {
                if (response.isSuccessful) {
                    Log.d("response", response.body().toString())
                    progress_bar.visibility = View.GONE
                    recyclerView.apply {
                        setHasFixedSize(true)
                        layoutManager = LinearLayoutManager(this@MainActivity)
                        adapter = response.body()?.let { PhotoAdapter(it) }
                    }
                }
            }

            override fun onFailure(call: Call<ListPhoto>, t: Throwable) {
                Toast.makeText(this@MainActivity, "${t.message}", Toast.LENGTH_LONG).show()
            }
        })
    }
}
