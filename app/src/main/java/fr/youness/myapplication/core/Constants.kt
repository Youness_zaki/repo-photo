package fr.youness.myapplication.core

const val PACKAGE_NAME = "fr.youness.myapplication"
const val BASE_URL = "https://picsum.photos/"
const val EMPLOYEES_URL = BASE_URL + "/v2/list"
const val TABULATION = "\t"
const val PIXEL_UNITE = "px"
const val EXTRA_PHOTO = "extra_photo"
