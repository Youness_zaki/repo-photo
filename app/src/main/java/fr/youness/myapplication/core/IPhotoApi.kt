package fr.youness.myapplication.core

import fr.youness.myapplication.model.ListPhoto
import retrofit2.Call
import retrofit2.http.GET

interface IPhotoApi {
    @GET(EMPLOYEES_URL)
    fun getEmployees () : Call<ListPhoto>
}