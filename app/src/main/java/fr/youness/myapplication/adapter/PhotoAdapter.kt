package fr.youness.myapplication.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.youness.myapplication.DetailPhotoActivity
import fr.youness.myapplication.R
import fr.youness.myapplication.adapter.PhotoAdapter.PhotoViewHolder
import fr.youness.myapplication.core.EXTRA_PHOTO
import fr.youness.myapplication.core.PIXEL_UNITE
import fr.youness.myapplication.core.TABULATION
import fr.youness.myapplication.model.Photo

class PhotoAdapter (val photos: List<Photo>): RecyclerView.Adapter<PhotoViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val inflater = LayoutInflater.from(parent!!.context)
        val view = inflater.inflate(R.layout.photo_item, parent, false)
        return PhotoViewHolder(view)
    }

    class PhotoViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){
        private val photo:ImageView = itemView.findViewById(R.id.photo_picture)
        private val id:TextView = itemView.findViewById(R.id.photo_id)
        private val author:TextView = itemView.findViewById(R.id.photo_author)
        private val width:TextView = itemView.findViewById(R.id.photo_width)
        private val hight:TextView = itemView.findViewById(R.id.photo_hight)

        fun bind(photo: Photo) {
            Glide.with(itemView.context).load(photo.download_url).into(this.photo)
            id.text = id.text as String? + TABULATION + photo.id
            author.text = author.text as String? + TABULATION + photo.author
            width.text = width.text as String? + TABULATION + photo.width.toString() + PIXEL_UNITE
            hight.text = hight.text as String? + TABULATION + photo.height.toString() + PIXEL_UNITE
        }

    }

    override fun getItemCount(): Int {
        return photos.size
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val currentPhoto = photos[position];
        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context,DetailPhotoActivity::class.java)
            intent.putExtra(EXTRA_PHOTO, currentPhoto)
            holder.itemView.context.startActivity(intent)
        }
        return holder.bind(currentPhoto)
    }
}