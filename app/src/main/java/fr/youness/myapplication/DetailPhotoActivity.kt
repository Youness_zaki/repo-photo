package fr.youness.myapplication

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import fr.youness.myapplication.core.EXTRA_PHOTO
import fr.youness.myapplication.core.PIXEL_UNITE
import fr.youness.myapplication.core.TABULATION
import fr.youness.myapplication.model.Photo
import kotlinx.android.synthetic.main.activity_detail_photo.*


class DetailPhotoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_photo)
        setSupportActionBar(toolbar)
//        Afficher la flèche pour revenir à l'activité précédente
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
//        Récupérer l'objet Photo
        val photo = intent.getSerializableExtra(EXTRA_PHOTO) as? Photo
//        Binding
        setComponent(photo)
    }

    private fun setComponent(photo: Photo?) {
        val picture: ImageView = findViewById(R.id.photo_picture)
        val id: TextView = findViewById(R.id.photo_id)
        val author: TextView = findViewById(R.id.photo_author)
        val width: TextView = findViewById(R.id.photo_width)
        val hight: TextView = findViewById(R.id.photo_hight)
        if (photo != null) {
            Glide.with(this).load(photo.download_url).into(picture)
        }
        if (photo != null) {
            id.text = id.text as String? + TABULATION + photo.id
        }
        if (photo != null) {
            author.text = author.text as String? + TABULATION + photo.author
        }
        if (photo != null) {
            width.text = width.text as String? + TABULATION + photo.width.toString() + PIXEL_UNITE
        }
        if (photo != null) {
            hight.text = hight.text as String? + TABULATION + photo.height.toString() + PIXEL_UNITE
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
