package fr.youness.myapplication.model

import java.io.Serializable

data class Photo(
    val id: String?, // 0
    val author: String?, // Alejandro Escamilla
    val width: Int?, // 5616
    val height: Int?, // 3744
    val url: String?, // https://unsplash.com/photos/yC-Yzbqy7PY
    val download_url: String? // https://picsum.photos/id/0/5616/3744
) : Serializable